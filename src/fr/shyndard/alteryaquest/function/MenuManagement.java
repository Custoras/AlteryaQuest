package fr.shyndard.alteryaquest.function;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.shyndard.alteryaquest.Main;
import fr.shyndard.alteryaquest.method.NPCQuest;
import fr.shyndard.alteryaquest.method.PlayerQuest;
import fr.shyndard.alteryaquest.method.Quest;
import net.citizensnpcs.api.CitizensAPI;

public class MenuManagement {
	
	public static void openGlobal(Player player) {
		PlayerQuest pq = Main.getPlayerList().get(player);
		int line = Math.round(pq.getQuestList().size()/9);
		Inventory inv = Bukkit.createInventory(player, (line+1)*9, Main.getQuestMenuPrefix());
		pq.getGlobalMenuList().clear();
		int index = 0;
		for(Integer i : Main.getPlayerList().get(player).getQuestList()) {
			addQuest(Main.getQuestList().get(i), inv, player, true, index);
			pq.addGlobalMenu(index, i);
			index++;
		}
		player.openInventory(inv);
	}

	public static void openEnd(Player player, NPCQuest npc) {
		PlayerQuest pq = Main.getPlayerList().get(player);
		pq.setMenuEnd();
		Inventory inv = Bukkit.createInventory(player, 9,Main.getNPCMenuPrefix() + CitizensAPI.getNPCRegistry().getById(npc.getId()).getName());
		for(Integer quest_id : Main.getPlayerList().get(player).getQuestList()) {
			if(npc.getQuestList().contains(quest_id)) {
				Quest quest = Main.getQuestList().get(quest_id);
				if(quest.checkCompleted(player, false) && quest.getEndNpcId() == npc.getId()) {
					addQuest(quest, inv, player, true, null);
					pq.getMenuSlot().add(quest.getId());
				}
			}
		}
		player.openInventory(inv);
	}
	
	public static void openList(Player player, NPCQuest npc) {
		PlayerQuest pq = Main.getPlayerList().get(player);
		pq.setMenuList();
		Inventory inv = Bukkit.createInventory(player, 9, Main.getNPCMenuPrefix() + CitizensAPI.getNPCRegistry().getById(npc.getId()).getName());
		List<Integer> quest_list = SQLFunction.getQuestList(player, npc);
		if(quest_list.size() == 0) {
			player.sendMessage(ChatColor.DARK_PURPLE + CitizensAPI.getNPCRegistry().getById(npc.getId()).getName() + ChatColor.GRAY + "> Je n'ai aucune t�che � te confier pour le moment.");
		} else {
			for(Integer quest_id : quest_list) {
				Quest quest = Main.getQuestList().get(quest_id);
				if(npc.getId() == quest.getDeliveryNpcID()) {
					addQuest(quest, inv, player, false, null);
					pq.getMenuSlot().add(quest.getId());
				}
			}
			player.openInventory(inv);
		}
	}
	
	private static void addQuest(Quest quest, Inventory inv, Player player, boolean checkValideTask, Integer index) {
		ItemStack is = new ItemStack(Material.MAP);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + quest.getName());
		im.setLore(quest.getDescription(player, checkValideTask));
		is.setItemMeta(im);
		if(index == null) inv.addItem(is);
		else inv.setItem(index, is);
	}
}