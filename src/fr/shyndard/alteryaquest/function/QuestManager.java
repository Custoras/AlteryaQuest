package fr.shyndard.alteryaquest.function;

import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.Title;
import fr.shyndard.alteryaquest.Main;
import fr.shyndard.alteryaquest.method.Quest;
import fr.shyndard.alteryaquest.method.Task;
import fr.shyndard.alteryaquest.method.TaskCategory;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.ChatColor;

public class QuestManager {
	
	public static void stopQuest(Player player, Quest quest) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("DELETE FROM quest_player WHERE quest_end_time IS NULL AND quest_id = " + quest.getId() + " AND player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"')");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
		Main.getPlayerList().get(player).loadQuest();
		player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1f, 1f);
		player.sendMessage(Main.getQuestPrefix() + "Vous avez " + ChatColor.RED + "abandonn�" + ChatColor.GRAY + " la qu�te " + ChatColor.AQUA + quest.getName() + ChatColor.GRAY + " !");
	}
	
	public static void endOfPlayerQuest(Player player, Quest quest) {
		player.closeInventory();
		Main.getPlayerList().get(player).getQuestList().remove(quest.getId());
		if(SQLFunction.alreadyCompleted(player, quest)) {
			player.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous avez d�j� compl�t� cette qu�te.");
		} else {
			try {
				Statement state = DataAPI.getSql().getConnection().createStatement();
				state.executeUpdate("UPDATE quest_player SET quest_end_time = '" + System.currentTimeMillis()/1000 + "' WHERE quest_id = " + quest.getId() + " AND player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"')");
				state.close();
			} catch (SQLException e) {e.printStackTrace();}
			new Title(ChatColor.GOLD + "Qu�te termin�e !", "", 5, 40, 20).send(player);
			player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1f, 1f);
			player.sendMessage(Main.getQuestPrefix() + "Vous avez termin� la qu�te " + ChatColor.AQUA + quest.getName() + ChatColor.GRAY + " !");
		}
	}
	
	public static boolean checkOneQuestCompleted(Player player, Integer npc_id) {
		for(Integer index : Main.getPlayerList().get(player).getQuestList()) {
			Quest quest = Main.getQuestList().get(index);
			if(quest.checkCompleted(player, false) && quest.getEndNpcId() == npc_id) return true;
		}
		return false;
	}
	
	public static boolean actionNpcTalkTask(NPC npc, Player player) {
		for(Integer index : Main.getPlayerList().get(player).getQuestList()) {
			Quest quest = Main.getQuestList().get(index);
			for(Task t : quest.getTaskList()) {
				if(t.getCategory() == TaskCategory.TALKTONPC.getValue()) {
					if(!SQLFunction.hasValideTask(index, t.getId(), player)) {
						boolean canContinue = true;
						try {
							if(Integer.parseInt(t.getData()[0]) != npc.getId()) canContinue = false;
						} catch(Exception ex) {
							canContinue = false;
						}
						if(canContinue) {
							SQLFunction.setValideTask(index, t.getId(), player);
							t.execute(npc, player);
							return true;
						}
					}
					System.out.println(npc.getFullName() + " 4");
				}
			}
		}
		return false;
	}

	public static void playerMove(Player player, Location loc) {
		for(Integer index : Main.getPlayerList().get(player).getQuestList()) {
			Quest quest = Main.getQuestList().get(index);
			for(Task t : quest.getTaskList()) {
				if(t.getCategory() == TaskCategory.GOTOAREA.getValue()) {
					if(!SQLFunction.hasValideTask(index, t.getId(), player)) {
						if(t.getData().length == 4) {
							if(t.getCenter().getWorld() == loc.getWorld() && t.getCenter().distance(loc) <= 4) {
								SQLFunction.setValideTask(index, t.getId(), player);
							}
						} else if(t.getData().length == 6) {
							String args[] = t.getData();
							if(args[4].equals("entity")) {
								for(Entity e : t.getCenter().getWorld().getNearbyEntities(t.getCenter(), 4, 2, 4)) {
									if(e.getType().ordinal() == Integer.parseInt(args[5])) {
										SQLFunction.setValideTask(index, t.getId(), player);
										return;
									}
								}
							} else if(args[4].equals("npc")) {
								if(CitizensAPI.getNPCRegistry().getById(Integer.parseInt(args[5])).getStoredLocation().distance(t.getCenter()) <= 4) {
									SQLFunction.setValideTask(index, t.getId(), player);
									return;
								}
							}
						}
					}
				}
			}
		}
	}
}