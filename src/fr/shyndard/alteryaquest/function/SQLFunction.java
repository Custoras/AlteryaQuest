package fr.shyndard.alteryaquest.function;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaquest.Main;
import fr.shyndard.alteryaquest.method.NPCQuest;
import fr.shyndard.alteryaquest.method.PlayerQuest;
import fr.shyndard.alteryaquest.method.Quest;
import fr.shyndard.alteryaquest.method.Reward;
import fr.shyndard.alteryaquest.method.Task;

public class SQLFunction {

	public static void loadQuestList() {
		Main.clearQuestList();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT * FROM quest_list WHERE quest_visible = 1");
			while(result.next()) {
				try {
					Main.getPlugin().getLogger().info("Load quest " + result.getInt("quest_id"));
					Main.addQuest(new Quest(result.getInt("quest_id"), result.getString("quest_name"), result.getString("quest_note"), result.getString("quest_speech"), result.getInt("quest_require_quest_id"), result.getInt("quest_require_rank_id"), result.getBoolean("quest_multi"), result.getInt("quest_delivery_npc_id"), result.getInt("quest_end_npc_id"), result.getInt("quest_interval_repeat")));
				} catch (Exception ex) { ex.printStackTrace(); }
			}
		} catch (SQLException e) {e.printStackTrace();}
		loadNCPList();
	}
	
	public static void loadTask(Quest quest) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT * FROM quest_task WHERE quest_id = " + quest.getId() + " AND quest_task_category IS NOT NULL");
			while(result.next()) {
				try {
					quest.addTask(new Task(result.getInt("quest_task_id"), result.getInt("quest_task_category"), result.getString("quest_task_data"), quest.getId()));
				} catch (Exception ex) { ex.printStackTrace(); }
			}
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static void loadReward(Quest quest) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT * FROM quest_reward WHERE quest_id = " + quest.getId() + " AND quest_reward_category IS NOT NULL");
			while(result.next()) {
				quest.addReward(new Reward(result.getInt("quest_reward_category"), result.getString("quest_reward_data")));
			}
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	public static void loadNCPList() {
		Main.clearNPC();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT quest_id, quest_delivery_npc_id, quest_end_npc_id FROM quest_list WHERE quest_delivery_npc_id IS NOT NULL OR quest_end_npc_id IS NOT NULL");
			while(result.next()) {
				npcAddQuest(result.getInt("quest_delivery_npc_id"), result.getInt("quest_id"));
				npcAddQuest(result.getInt("quest_end_npc_id"), result.getInt("quest_id"));
			}
		} catch (SQLException e) {e.printStackTrace();}
	}
	
	static void npcAddQuest(Integer npc_id, Integer quest_id) {
		if(Main.getNpcQuestList().get(npc_id) == null) {
			Main.addNPCQuest(new NPCQuest(npc_id));
		}
		Main.getNpcQuestList().get(npc_id).addQuest(quest_id);
	}
	
	public static void setStartPlayerQuest(Player player, Integer quest_id) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("INSERT INTO quest_player (player_id, quest_id, quest_start_time) VALUES ((SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"'), "+quest_id+", '"+System.currentTimeMillis()/1000+"')");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static List<Integer> getQuestList(Player player, NPCQuest npc) {
		List<Integer> list = new ArrayList<>();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT quest_id FROM quest_player_available WHERE uuid = '" + player.getUniqueId() + "' AND quest_delivery_npc_id = " + npc.getId() + " AND quest_require_rank_id >= " + DataAPI.getPlayer(player).getRank().getId());
			while(result.next()) {
				list.add(result.getInt("quest_id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static boolean alreadyRegistred(Player player, Quest quest) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM quest_player_available WHERE quest_id = " + quest.getId() + " AND uuid = '" + player.getUniqueId() + "'");
			while(result.next()) {
				return false;
			}
		} catch (SQLException e) {e.printStackTrace();}
		return true;
	}
	
	public static boolean alreadyCompleted(Player player, Quest quest) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM quest_player_available WHERE quest_id = " + quest.getId() + " AND uuid = '" + player.getUniqueId() + "'");
			while(result.next()) {
				return true;
			}
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}
	
	public static boolean loadPlayerQuestList(Player player, PlayerQuest pq) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT QL.quest_id FROM quest_player AS QP, quest_list AS QL WHERE quest_visible = 1 AND player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') AND quest_end_time IS NULL AND QP.quest_id = QL.quest_id ");
			while(result.next()) {
				pq.addQuest(result.getInt("quest_id"));
			}
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}

	public static void reset(Player player) {
		Main.getPlayerList().get(player).getQuestList().clear();
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("DELETE FROM quest_player WHERE player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') AND quest_id > 1");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static int getKillCount(Player player, Integer entity_type, Integer quest_id) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT count(stats_id) AS count FROM stats_kill_entity AS ST, quest_player AS QP WHERE ST.player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') AND QP.player_id = ST.player_id AND QP.quest_id = "+quest_id+" AND stats_timestamp >= quest_start_time AND stats_entity_type_id = " + entity_type);
			while(result.next()) {
				return result.getInt("count");
			}
		} catch (SQLException e) {e.printStackTrace();}
		return 0;
	}

	public static void addType(EntityType et) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("INSERT INTO entity_type VALUES ("+et.ordinal()+", '"+et.name()+"')");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}

	public static int getItemCraftCount(Player player, String type, String data, Integer quest_id) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT SUM(item_count) AS count FROM stats_craft_item AS SCI, quest_player AS QP WHERE SCI.player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') AND QP.player_id = SCI.player_id AND QP.quest_id = "+quest_id+" AND stats_timestamp >= quest_start_time AND item_type = '" + Material.matchMaterial(type) + "' AND item_data = " + data);
			while(result.next()) {
				return result.getInt("count");
			}
		} catch (SQLException e) {e.printStackTrace();}
		return 0;
	}

	public static boolean hasPlotInTown(Player player, int city_id) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM area_plot WHERE area_city_id = " + city_id + " AND area_plot_owner_player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"')");
			while(result.next()) return true;
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}

	public static boolean hasValideTask(Integer quest_id, Integer task_id, Player player) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			ResultSet result = state.executeQuery("SELECT 1 FROM quest_task_player AS QTP, quest_player AS QP WHERE QTP.player_id = (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"') AND QP.player_id = QTP.player_id AND QP.quest_id = "+quest_id+" AND timestamp >= quest_start_time AND task_id = " + task_id);
			while(result.next()) return true;
		} catch (SQLException e) {e.printStackTrace();}
		return false;
	}

	public static void setValideTask(Integer quest_id, Integer task_id, Player player) {
		try {
			Statement state = DataAPI.getSql().getConnection().createStatement();
			state.executeUpdate("INSERT INTO quest_task_player VALUES ("+quest_id+", "+task_id+", (SELECT id FROM player_information WHERE uuid = '"+player.getUniqueId()+"'), "+(System.currentTimeMillis()/1000)+")");
			state.close();
		} catch (SQLException e) {e.printStackTrace();}
	}
}
