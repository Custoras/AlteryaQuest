package fr.shyndard.alteryaquest;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import fr.shyndard.alteryaquest.api.QuestAPI;
import fr.shyndard.alteryaquest.event.PlayerConnection;
import fr.shyndard.alteryaquest.event.PlayerInteractMenu;
import fr.shyndard.alteryaquest.event.PlayerInteractNPC;
import fr.shyndard.alteryaquest.event.PlayerMovement;
import fr.shyndard.alteryaquest.function.SQLFunction;
import fr.shyndard.alteryaquest.method.NPCQuest;
import fr.shyndard.alteryaquest.method.PlayerQuest;
import fr.shyndard.alteryaquest.method.Quest;
import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin {

	static Main plugin;
	static Map<Integer, Quest> quest_list = new HashMap<>();
	static Map<Player, PlayerQuest> player_list = new HashMap<>();
	static Map<Integer, NPCQuest> npc_list = new HashMap<>();
	
	public void onEnable() {
		plugin = this;
		
		getCommand("quest").setExecutor(new CmdQuest());
		
		getServer().getPluginManager().registerEvents(new PlayerConnection(), this);
		getServer().getPluginManager().registerEvents(new PlayerInteractMenu(), this);
		getServer().getPluginManager().registerEvents(new PlayerInteractNPC(), this);
		getServer().getPluginManager().registerEvents(new PlayerMovement(), this);
	    
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
			public void run() {
				 SQLFunction.loadQuestList();
			}
		}, 40L);
	    QuestAPI.init();
	}
	
	public static Main getPlugin() {
		return plugin;
	}
	
	public static Map<Integer, NPCQuest> getNpcQuestList() {
		return npc_list;
	}
	
	public static void addNPCQuest(NPCQuest npc) {
		npc_list.put(npc.getId(), npc);
	}
	
	public static Map<Player, PlayerQuest> getPlayerList() {
		return player_list;
	}
	
	public static void addPlayer(PlayerQuest pq, Player player) {
		player_list.put(player, pq);
	}
	
	public static Map<Integer, Quest> getQuestList() {
		return quest_list;
	}
	
	public static void addQuest(Quest quest) {
		quest_list.put(quest.getId(), quest);
	}
	
	public static String getQuestPrefix() {
		return ChatColor.WHITE + "[" + ChatColor.GREEN + ChatColor.BOLD + "Qu�te" + ChatColor.WHITE + "] " + ChatColor.GRAY;
	}
	
	public static String getQuestMenuPrefix() {
		return ChatColor.DARK_PURPLE + "Livre de qu�te";
	}
	
	public static String getNPCMenuPrefix() {
		return ChatColor.DARK_PURPLE + "Habitant ";
	}

	public static void clearQuestList() {
		quest_list.clear();
	}

	public static void clearNPC() {
		npc_list.clear();
	}
}
