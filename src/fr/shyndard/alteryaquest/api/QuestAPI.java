package fr.shyndard.alteryaquest.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.NPCAPI;
import net.citizensnpcs.api.npc.NPC;

public class QuestAPI {

	static Map<Integer, Long> spam_time = new HashMap<>();
	static List<String> message = new ArrayList<>();
	
	public static void init() {
		message.add("Beau temps n'est ce pas ?");
		message.add("Excuse moi, je n'ai pas le temps");
		message.add("Tu veux ma photo ?");
		message.add("J'ai d�j� donn� pour les calendriers");
		message.add("208, 209, 300 ! Hey toi l�, t'approches pas de mes " + DataAPI.getMoneySymbol());
	}
	
	public static boolean canInteractNPC(Player player) {
		if(spam_time.get(player.getEntityId()) != null) {
			if(spam_time.get(player.getEntityId()) + 5 >= System.currentTimeMillis()/1000) {
				player.sendMessage(ChatColor.RED + "N'emb�te pas les habitants...");
				return false;
			}
		}
		return true;
	}
	
	public static Map<Integer, Long> getSpamNPCTime() {
		return spam_time;
	}

	public static void interact(Player player) {
		spam_time.put(player.getEntityId(), System.currentTimeMillis()/1000);
	}
	
	public static void sendRandomMessage(NPC npc, Player player) {
		NPCAPI.talkTo(npc, player, message.get(new Random().nextInt(message.size())));
	}
}
