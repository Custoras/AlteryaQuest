package fr.shyndard.alteryaquest.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import fr.shyndard.alteryaquest.Main;
import fr.shyndard.alteryaquest.method.PlayerQuest;

public class PlayerConnection implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Main.addPlayer(new PlayerQuest(event.getPlayer()), event.getPlayer());
	}
}
