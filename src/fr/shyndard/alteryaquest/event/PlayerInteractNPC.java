package fr.shyndard.alteryaquest.event;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.NPCAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaquest.Main;
import fr.shyndard.alteryaquest.api.QuestAPI;
import fr.shyndard.alteryaquest.function.QuestManager;
import fr.shyndard.alteryaquest.function.SQLFunction;
import fr.shyndard.alteryaquest.method.NPCQuest;
import fr.shyndard.alteryaquest.method.Quest;
import net.citizensnpcs.api.event.NPCLeftClickEvent;

public class PlayerInteractNPC implements Listener {
	
	@EventHandler
	public void onInteract(NPCLeftClickEvent event) {
		if(event.isCancelled()) return;
		PlayerInformation pi = DataAPI.getPlayer(event.getClicker());
		if(!pi.hasPermission("quest.access")) return;
		Player player = event.getClicker();
		if(QuestManager.actionNpcTalkTask(event.getNPC(), player)) return;
		NPCQuest npc = Main.getNpcQuestList().get(event.getNPC().getId());
		if(npc == null) return;
		List<Integer> quest_list = SQLFunction.getQuestList(player, npc);
		boolean npcHasQuestToDelivery = quest_list.size() > 0;
		boolean npcHasQuestToReceive = QuestManager.checkOneQuestCompleted(player, event.getNPC().getId());
		if(!npcHasQuestToDelivery && !npcHasQuestToReceive) QuestAPI.sendRandomMessage(event.getNPC(), player);
		else {
			if(!npcHasQuestToReceive && quest_list.size() == 1) {
				Quest quest = Main.getQuestList().get(quest_list.get(0));
				NPCAPI.talkTo(event.getNPC(), player, quest.getSpeech(), quest.getId());
			} else {
				NPCAPI.talkTo(event.getNPC(), player, "Salutations voyageur, que puis-je pour toi ?");
				if(npcHasQuestToDelivery) {
					NPCAPI.answer(player, "Avez vous quelque chose pour moi ?", "/quest talknpc " + npc.getId() + " list");
				}
				if(npcHasQuestToReceive) {
					NPCAPI.answer(player, "J'ai une qu�te � vous rendre", "/quest talknpc " + npc.getId() + " end");
				}
			}
		}
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onInteractManager(NPCLeftClickEvent event) {
		if(QuestAPI.canInteractNPC(event.getClicker())) QuestAPI.interact(event.getClicker());
		else event.setCancelled(true);
	}
}