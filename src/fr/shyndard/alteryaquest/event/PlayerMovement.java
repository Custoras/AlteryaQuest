package fr.shyndard.alteryaquest.event;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import fr.shyndard.alteryaquest.function.QuestManager;

public class PlayerMovement implements Listener {
	
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		if((event.getFrom().getBlockX() != event.getTo().getBlockX()
				|| event.getFrom().getBlockY() != event.getTo().getBlockY()
				|| event.getFrom().getBlockZ() != event.getTo().getBlockZ())
				&& event.getFrom().getWorld() == event.getTo().getWorld())
			QuestManager.playerMove(event.getPlayer(), event.getTo());
	}

}
