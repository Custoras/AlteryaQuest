package fr.shyndard.alteryaquest.event;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.shyndard.alteryaapi.api.FunctionAPI;
import fr.shyndard.alteryaapi.api.NPCAPI;
import fr.shyndard.alteryaquest.Main;
import fr.shyndard.alteryaquest.method.PlayerQuest;
import fr.shyndard.alteryaquest.method.Quest;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;

public class PlayerInteractMenu implements Listener {

	@EventHandler
	public void onInteractGlobalMenu(InventoryClickEvent event) {
		if(event.getClickedInventory() == null) return;
		if(!event.getClickedInventory().getName().equals(Main.getQuestMenuPrefix())) return;
		if(event.getClick().isRightClick()) {
			Player player = (Player)event.getWhoClicked();
			int quest_id = Main.getPlayerList().get(player).getGlobalMenuList().get(event.getSlot());
			Quest quest = Main.getQuestList().get(quest_id);
			System.out.println(quest_id);
			FunctionAPI.question(player, ChatColor.GREEN + " - Confirmer l'abandon de " + ChatColor.AQUA + quest.getName() + ChatColor.GREEN + " ?", "/quest abandonner " + quest.getId());
			player.closeInventory();
		}
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onInteract(InventoryClickEvent event) {
		if(event.getClickedInventory() == null) return;
		if(event.getView().getTopInventory() != null 
				&& event.getView().getTopInventory().getName().startsWith(Main.getNPCMenuPrefix()) 
				&& !event.getClickedInventory().getName().startsWith(Main.getNPCMenuPrefix())
				&& event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY)
		{
			event.setCancelled(true);
			return;
		}
		if(!event.getClickedInventory().getName().startsWith(Main.getNPCMenuPrefix())) return;
		event.setCancelled(true);
		Player player = (Player)event.getWhoClicked();
		PlayerQuest pq = Main.getPlayerList().get(player);
		try {
			pq.getMenuSlot().get(event.getSlot());
		} catch(Exception ex) { return; }
		if(pq.isMenuList()) {
			Quest quest = Main.getQuestList().get(pq.getMenuSlot().get(event.getSlot()));
			NPC npc = CitizensAPI.getNPCRegistry().getById(quest.getDeliveryNpcID());
			NPCAPI.talkTo(npc, player, quest.getSpeech(), quest.getId());
			player.closeInventory();
		} else if(pq.isMenuEnd()) {
			if(!Main.getQuestList().get(pq.getMenuSlot().get(event.getSlot())).checkCompleted(player, true)) {
				player.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous ne remplissez pas les conditions.");
			}
		}
	}
}
