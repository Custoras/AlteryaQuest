package fr.shyndard.alteryaquest.method;

import java.util.ArrayList;
import java.util.List;

public class NPCQuest {

	private Integer npc_id;
	private List<Integer> quest_list = new ArrayList<>();
	
	public NPCQuest(Integer npc_id) {
		this.npc_id = npc_id;
	}
	
	public Integer getId() {
		return npc_id;
	}
	
	public List<Integer> getQuestList() {
		return quest_list;
	}

	public void addQuest(Integer quest_id) {
		if(quest_list.contains(quest_id)) return;
		quest_list.add(quest_id);
	}
}