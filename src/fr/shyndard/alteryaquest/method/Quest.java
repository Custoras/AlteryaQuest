package fr.shyndard.alteryaquest.method;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import fr.shyndard.alteryaquest.function.QuestManager;
import fr.shyndard.alteryaquest.function.SQLFunction;
import net.citizensnpcs.api.CitizensAPI;
import net.md_5.bungee.api.ChatColor;

public class Quest {

	private Integer id;
	private String name;
	private String speech;
	private String note;
	private Integer require_quest_id;
	private Integer require_rank_id;
	private Integer delivery_npc_id;
	private Integer end_npc_id;
	private Integer interval_repeat;
	private boolean followquest;
	private List<Task> task_list = new ArrayList<>();
	private List<Reward> reward_list = new ArrayList<>();
	
	public Quest(Integer id, String name, String note, String speech, Integer require_quest_id, Integer require_rank_id, boolean followquest, Integer delivery_npc_id, Integer end_npc_id, Integer interval_repeat) {
		this.id = id;
		this.name = name;
		this.note = note;
		this.speech = speech;
		this.followquest = followquest;
		this.require_quest_id = require_quest_id;
		this.require_rank_id = require_rank_id;
		this.delivery_npc_id = delivery_npc_id;
		this.end_npc_id = end_npc_id;
		this.interval_repeat = interval_repeat;
		SQLFunction.loadTask(this);
		SQLFunction.loadReward(this);
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSpeech() {
		return speech;
	}
	
	public Integer getRequireQuestId() {
		return require_quest_id;
	}
	
	public Integer getRequireRankId() {
		return require_rank_id;
	}
	
	public Integer getDeliveryNpcID() {
		return delivery_npc_id;
	}
	
	public Integer getEndNpcId() {
		return end_npc_id;
	}
	
	public List<String> getDescription(Player player, boolean checkValideTask) {
		List<String> description = new ArrayList<>();
		if(followquest) {
			description.add(ChatColor.DARK_GREEN + "Suite de qu�te");
		}
		if(interval_repeat > 0) {
			if(interval_repeat == 1) description.add(ChatColor.DARK_GREEN + "Qu�te journali�re");
			else description.add(ChatColor.DARK_GREEN + "Reproductible tous les " + interval_repeat + " jours");
		}
		if(task_list.size() > 0) {
			description.add(null);
			description.add(ChatColor.AQUA + "N�c�ssaire pour valider la qu�te :");
			for(Task task : task_list) {
				description.add(ChatColor.GRAY + " - " + (checkValideTask ? (task.isCompleted(player) ? ChatColor.GREEN : ChatColor.RED) : "") + task.getDescription());
			}
		}
		if(reward_list.size() > 0) {
			description.add(null);
			description.add(ChatColor.AQUA + "R�compense :");
			for(Reward reward : reward_list) {
				description.add(ChatColor.GRAY + " - " + reward.getDescription());
			}
		}
		if(note != null && note.length() > 1) {
			description.add(null);
			String[] args = note.split("!�");
			description.add(ChatColor.GRAY + "Note : " + args[0]);
			for(int i = 1; i < args.length; i++) {
				description.add(ChatColor.GRAY + args[i]);
			}
		}
		description.add(null);
		description.add(ChatColor.AQUA + "Rendre � " + ChatColor.DARK_PURPLE + CitizensAPI.getNPCRegistry().getById(end_npc_id).getName());
		return description;
	}

	public List<Task> getTaskList() {
		return task_list;
	}
	
	public void addTask(Task task) {
		task_list.add(task);
	}
	
	public List<Task> getRewardList() {
		return task_list;
	}
	
	public void addReward(Reward reward) {
		reward_list.add(reward);
	}

	public boolean checkCompleted(Player player, boolean valide) {
		boolean completed = true;
		for(Task task : task_list) {
			if(!task.isCompleted(player)) {
				completed = false;
				break;
			}
		}
		if(completed && valide) {
			for(Task task : task_list) {
				task.removeTo(player);
			}
			for(Reward reward : reward_list) {
				reward.addTo(player);
			}
			QuestManager.endOfPlayerQuest(player, this);
		}
		return completed;
	}
}