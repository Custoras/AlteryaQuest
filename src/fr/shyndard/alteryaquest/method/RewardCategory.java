package fr.shyndard.alteryaquest.method;

public enum RewardCategory {
	
	RANK(0, "rank"),
	RESOURCE(1, "resource"),
	MONEY(2, "money");
	
	Integer value;
	String name;
	
	private RewardCategory(Integer value, String name) {
		this.value = value;
		this.name= name;
	}
	
	public Integer getValue() {
		return this.value;
	}
	
	public String getName() {
		return this.name;
	}
	
	public static RewardCategory get(Integer value) {
		for(RewardCategory status : RewardCategory.values()) {
			if(status.getValue() == value) {
				return status;
			}
		}
		return null;
	}
}