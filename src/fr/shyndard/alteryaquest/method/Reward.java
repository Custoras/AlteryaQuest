 package fr.shyndard.alteryaquest.method;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.meowj.langutils.lang.LanguageHelper;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaquest.Main;
import net.md_5.bungee.api.ChatColor;

public class Reward {

	private Integer category;
	private String data;
	private String description;
	
	public Reward(Integer category, String data) {
		this.category = category;
		this.data = data;
		setDescription();
	}
	
	public void addTo(Player player) {
		if(category == RewardCategory.RANK.getValue()) {
			String[] args = data.split(":");
			PlayerInformation pi = DataAPI.getPlayer(player);
			pi.setRank(Integer.parseInt(args[0]), true);
			if(args[1].equals("1")) Bukkit.broadcastMessage(player.getDisplayName() + ChatColor.GOLD + " est pass�(e) au grade " + pi.getRank().getColor() + pi.getRank().getName());
		} else if(category == RewardCategory.MONEY.getValue()) {
			PlayerInformation pi = DataAPI.getPlayer(player);
			Float value = Float.parseFloat(data);
			if(value + pi.getMoney(false) > pi.getMaxMoney()) {
				Float temp_value = pi.getMaxMoney()-pi.getMoney(false);
				pi.addMoney(temp_value, true, false);
				player.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous n'avez pas assez de place dans votre bourse.");
				pi.addMoney(value-temp_value, true, true);
			} else pi.addMoney(value, true, false);
		} else if(category == RewardCategory.RESOURCE.getValue()) {
			String[] args = data.split(":");
			Integer count = 0;
			int max = Integer.parseInt(args[2]);
			Material mat = Material.matchMaterial(args[0]);
			short data_byte = Short.parseShort(args[1]);
			while(count != max) {
				int value = (max - count);
				if(value > 64) value = 64;
				count += value;
				player.getInventory().addItem(new ItemStack(mat, value, data_byte));
			}
		}
	}

	public String getDescription() {
		return description;
	}
	
	private void setDescription() {
		if(category == RewardCategory.RANK.getValue()) {
			String[] args = data.split(":");
			description = "Grade : " + DataAPI.getRankList().get(Integer.parseInt(args[0])).getName();
		}
		else if(category == RewardCategory.MONEY.getValue()) {
			description = "Argent : " + Float.parseFloat(data) + DataAPI.getMoneySymbol();
		}
		else if(category == RewardCategory.RESOURCE.getValue()) {
			String[] args = data.split(":");
			description = "Ressource : " + Integer.parseInt(args[2]) + "x " + LanguageHelper.getItemName(new ItemStack(Material.matchMaterial(args[0]), 1, Short.parseShort(args[1])), "fr_FR");
		}
	}
}