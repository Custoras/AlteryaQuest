package fr.shyndard.alteryaquest.method;

public enum TaskCategory {
	
	GETRANK(0, "get rank"),
	RESOURCE(1, "resource"),
	MONEY(2, "money"),
	KILL_ENTITY(3, "kill entity"),
	LEVEL(5, "level"),
	CRAFT(6, "craft"),
	CRAFTANDREPORT(7, "craft + ressource"),
	HASPLOTINTOWN(8, "has plot in town"),
	GOTOAREA(9, "go to an area"),
	TALKTONPC(10, "talk to npc");
	
	Integer value;
	String name;
	
	private TaskCategory(Integer value, String name) {
		this.value = value;
		this.name= name;
	}
	
	public Integer getValue() {
		return this.value;
	}
	
	public String getName() {
		return this.name;
	}
	
	public static TaskCategory get(Integer value) {
		for(TaskCategory status : TaskCategory.values()) {
			if(status.getValue() == value) {
				return status;
			}
		}
		return null;
	}
}