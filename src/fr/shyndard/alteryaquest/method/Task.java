package fr.shyndard.alteryaquest.method;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.meowj.langutils.lang.LanguageHelper;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.NPCAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaareamanagement.AreaManagementAPI;
import fr.shyndard.alteryaquest.function.SQLFunction;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.ChatColor;

public class Task {

	private Integer task_id;
	private Integer category;
	private String data;
	private Integer quest_id;
	private String require = "T�che incorrecte.";
	private Location area_center;
	
	public Task(Integer task_id, Integer category, String data, Integer quest_id) {
		this.task_id = task_id;
		this.category = category;
		this.data = data;
		this.quest_id = quest_id;
		setDescription();
	}
	
	public Integer getCategory() { return category; }
	
	public Integer getId() { return task_id; }
	
	public Location getCenter() { return area_center; }
	
	public String[] getData() { return data.split(":"); }
	
	@SuppressWarnings("deprecation")
	public boolean isCompleted(Player player) {
		if(category == TaskCategory.LEVEL.getValue()) {
			return (player.getLevel() >= Integer.parseInt(data));
		}
		if(category == TaskCategory.GETRANK.getValue()) {
			PlayerInformation pi = DataAPI.getPlayer(player);
			return (pi.getRank().getId() <= Integer.parseInt(data));
		}
		if(category == TaskCategory.MONEY.getValue()) {
			PlayerInformation pi = DataAPI.getPlayer(player);
			return (pi.getMoney(false) >= Float.parseFloat(data));
		}
		if(category == TaskCategory.CRAFT.getValue()) {
			String[] args = data.split(":");
			return (SQLFunction.getItemCraftCount(player, args[0], args[1], quest_id) >= Integer.parseInt(args[2]));
		}
		if(category == TaskCategory.RESOURCE.getValue()) {
			String[] args = data.split(":");
			int count = 0;
			Material mat = Material.matchMaterial(args[0]);
			short data_byte = Short.parseShort(args[1]);
			for(ItemStack is : player.getInventory().getContents()) {
				if(is != null && is.getType() == mat && is.getData().getData() == data_byte) {
					count += is.getAmount();
				}
			}
			return (count >= Integer.parseInt(args[2]));
		}
		if(category == TaskCategory.KILL_ENTITY.getValue()) {
			String[] args = data.split(":");
			return (SQLFunction.getKillCount(player, Integer.parseInt(args[0]), quest_id) >= Integer.parseInt(args[1]));
		}
		if(category == TaskCategory.CRAFTANDREPORT.getValue()) {
			String[] args = data.split(":");
			int count = 0;
			Material mat = Material.matchMaterial(args[0]);
			short data_byte = Short.parseShort(args[1]);
			for(ItemStack is : player.getInventory().getContents()) {
				if(is != null && is.getType() == mat && is.getData().getData() == data_byte) {
					count += is.getAmount();
				}
			}
			return (count >= Integer.parseInt(args[2]) && SQLFunction.getItemCraftCount(player, args[0], args[1], quest_id) >= Integer.parseInt(args[2]));
		}
		if(category == TaskCategory.HASPLOTINTOWN.getValue()) {
			String[] args = data.split(":");
			return (SQLFunction.hasPlotInTown(player, Integer.parseInt(args[0])));
		}
		if(category == TaskCategory.GOTOAREA.getValue() || category == TaskCategory.TALKTONPC.getValue()) {
			return SQLFunction.hasValideTask(quest_id, task_id, player);
		}
		return false;
	}
	
	public void removeTo(Player player) {
		if(category == TaskCategory.LEVEL.getValue()) {
			player.setLevel(player.getLevel() - Integer.parseInt(data));
		} else if(category == TaskCategory.MONEY.getValue()) {
			DataAPI.getPlayer(player).removeMoney(Float.parseFloat(data), true, false);
		} else if(category == TaskCategory.RESOURCE.getValue() || category == TaskCategory.CRAFTANDREPORT.getValue()) {
			String[] args = data.split(":");
			ItemStack is = new ItemStack(Material.matchMaterial(args[0]), 1, Short.parseShort(args[1]));
			removeItem(player, is, Integer.parseInt(args[2]));
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void removeItem(Player player, ItemStack item, int count) {
		int remove = 0;
		for(ItemStack is : player.getInventory().getContents()) {
			if(remove>= count) break;
			if(is != null && is.getType() != Material.AIR) {
				if(is.getType() == item.getType() && is.getData().getData() == item.getData().getData()) {
					if(remove + is.getAmount() > count) {
						is.setAmount(count-remove);
						break;
					} else {
						remove+=is.getAmount();
						is.setAmount(0);
					}
				}
			}
		}
		player.updateInventory();
	}

	public String getDescription() {
		return require;
	}
	
	public void setDescription() {
		if(category == TaskCategory.GETRANK.getValue()) {
			require = "Grade : " + DataAPI.getRankList().get(Integer.parseInt(data)).getName();
		}
		else if(category == TaskCategory.MONEY.getValue()) {
			require = "Argent : " + Float.parseFloat(data) + DataAPI.getMoneySymbol();
		}
		else if(category == TaskCategory.RESOURCE.getValue()) {
			String[] args = data.split(":");
			require = "Ressource : " + Integer.parseInt(args[2]) + "x " + LanguageHelper.getItemName(new ItemStack(Material.matchMaterial(args[0]), 1, Short.parseShort(args[1])), "fr_FR");
		}
		else if(category == TaskCategory.LEVEL.getValue()) {		
			require = "Niveau : " + data;
		} else if(category == TaskCategory.KILL_ENTITY.getValue()) {
			String[] args = data.split(":");
			require = "Tuer : " + Integer.parseInt(args[1]) + "x " + valueOf(Integer.parseInt(args[0]));
		} else if(category == TaskCategory.CRAFT.getValue()) {
			String[] args = data.split(":");
			require = "Crafter : " + Integer.parseInt(args[2]) + "x " + LanguageHelper.getItemName(new ItemStack(Material.matchMaterial(args[0]), 1, Short.parseShort(args[1])), "fr_FR");
		} else if(category == TaskCategory.CRAFTANDREPORT.getValue()) {
			String[] args = data.split(":");
			require = "Crafter et rapporter : " + Integer.parseInt(args[2]) + "x " + LanguageHelper.getItemName(new ItemStack(Material.matchMaterial(args[0]), 1, Short.parseShort(args[1])), "fr_FR");
		} else if(category == TaskCategory.HASPLOTINTOWN.getValue()) {
			require = "Posseder une parcelle dans : " + ChatColor.GOLD + AreaManagementAPI.getCityList().get(Integer.parseInt(data)).getName();
		} else if(category == TaskCategory.GOTOAREA.getValue()) {
			String[] args = data.split(":");
			area_center = new Location(Bukkit.getWorld(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]));
			if(area_center == null) return;
			require = area_center.getBlockX() + ", " + area_center.getBlockY() + ", " + area_center.getBlockZ();
			if(args.length == 6) {
				if(args[4].equals("entity")) {
					require = "Amenez 1x " + valueOf(Integer.parseInt(args[5])) + " pr�s de " + require;
				} else if(args[4].equals("npc")) {
					require = "Amenez " + CitizensAPI.getNPCRegistry().getById(Integer.parseInt(args[5])).getName() + " pr�s de " + require;
				}
			} else {
				require = "Allez pr�s de " + area_center.getBlockX() + ", " + area_center.getBlockY() + ", " + area_center.getBlockZ();
			}
		} else if(category == TaskCategory.TALKTONPC.getValue()) {
			String[] args = data.split(":");
			NPC npc = CitizensAPI.getNPCRegistry().getById(Integer.parseInt(args[0]));
			if(npc == null) return;
			require = "Parlez � " + npc.getFullName();
		}
		
	}
	
	static String valueOf(Integer value) {
		for(EntityType et : EntityType.values()) {
			if(et.ordinal() == value) return LanguageHelper.getEntityName(et, "fr_FR");;
		}
		return "inconnu";
	}

	public void execute(NPC npc, Player player) {
		String[] args = data.split(":");
		if(category == TaskCategory.TALKTONPC.getValue() && args.length > 1) {
			String msg = args[1];
			for(int i = 2; i < args.length; i++) msg+=args[i];
			NPCAPI.talkTo(npc, player, msg);
		}
	}
}