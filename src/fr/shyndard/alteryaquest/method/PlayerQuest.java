package fr.shyndard.alteryaquest.method;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.method.Title;
import fr.shyndard.alteryaquest.Main;
import fr.shyndard.alteryaquest.function.SQLFunction;

public class PlayerQuest {
	
	private List<Integer> player_quest_list = new ArrayList<>();
	private Player player;
	private short menu;
	private List<Integer> menu_slot = new ArrayList<>();
	private Map<Integer, Integer> globalMenu = new HashMap<>();
	
	public PlayerQuest(Player player) {
		this.player = player;
		loadQuest();
	}
	
	public List<Integer> getQuestList() {
		return player_quest_list;
	}
	
	public List<Integer> getMenuSlot() {
		return menu_slot;
	}
	
	public void addQuest(Integer quest_id) {
		player_quest_list.add(quest_id);
	}
	
	public boolean acceptQuest(Quest quest) {
		if(player_quest_list.size() >= 5) {
			player.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous ne pouvez avoir plus de 5 qu�tes simultanement.");
			return false;
		}
		if(quest == null) {
			player.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Impossible d'accepter cette qu�te.");
			return false;
		}
		if(quest.getRequireRankId() < DataAPI.getPlayer(player).getRank().getId()) {
			player.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous n'avez pas le grade pour accepter cette qu�te.");
			return false;
		}
		if(SQLFunction.alreadyRegistred(player, quest)) {
			player.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous avez d�j� accept� cette qu�te.");
			return false;
		}
		player_quest_list.add(quest.getId());
		SQLFunction.setStartPlayerQuest(player, quest.getId());
		new Title(ChatColor.GOLD + "Qu�te accept�e !", "", 5, 40, 20).send(player);
		player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1f, 1f);
		player.sendMessage(Main.getQuestPrefix() + ChatColor.GRAY + "Qu�te " + ChatColor.GREEN + ChatColor.ITALIC + quest.getName() + ChatColor.GRAY + " accept�e !");
		player.sendMessage(Main.getQuestPrefix() + ChatColor.GRAY + "Tapez /quete pour ouvrir votre livre de qu�te.");
		return true;
	}
	
	public boolean acceptQuest(Integer quest_id) {
		return acceptQuest(Main.getQuestList().get(quest_id));
	}

	public boolean isMenuEnd() {
		return (menu == 0);
	}
	
	public boolean isMenuList() {
		return (menu == 1);
	}
	
	public void setMenuEnd() {
		menu_slot.clear();
		menu = 0;
	}

	public void setMenuList() {
		menu_slot.clear();
		menu = 1;
	}

	public void checkQuest() {
		for(Integer index : player_quest_list) {
			if(Main.getQuestList().get(index) == null) {
				player_quest_list.remove(index);
			}
		}
	}

	public void loadQuest() {
		player_quest_list.clear();
		SQLFunction.loadPlayerQuestList(player, this);
	}

	public void addGlobalMenu(int slot, int quest_id) {
		globalMenu.put(slot, quest_id);
	}
	
	public Map<Integer, Integer> getGlobalMenuList() {
		return globalMenu;
	}
}