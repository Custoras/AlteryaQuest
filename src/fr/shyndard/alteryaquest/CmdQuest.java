package fr.shyndard.alteryaquest;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.shyndard.alteryaapi.api.DataAPI;
import fr.shyndard.alteryaapi.api.NPCAPI;
import fr.shyndard.alteryaapi.method.PlayerInformation;
import fr.shyndard.alteryaquest.function.MenuManagement;
import fr.shyndard.alteryaquest.function.QuestManager;
import fr.shyndard.alteryaquest.function.SQLFunction;
import fr.shyndard.alteryaquest.method.NPCQuest;
import fr.shyndard.alteryaquest.method.PlayerQuest;
import fr.shyndard.alteryaquest.method.Quest;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.md_5.bungee.api.ChatColor;

public class CmdQuest implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("La console n'a pas la permission.");
			return true;
		}
		Player player = (Player)sender;
		PlayerInformation pi = DataAPI.getPlayer(player);
		if(!pi.hasPermission("quest.access")) {
			sender.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous n'avez pas la permission.");
			return true;
		}
		if(args.length == 2 && args[0].equals("abandonner")) {
			Quest quest;
			try {
				quest = Main.getQuestList().get(Integer.parseInt(args[1]));
			} catch(Exception ex) {
				sender.sendMessage(ChatColor.RED + "Qu�te inconnue, veuillez recommencer.");
				return true;
			}
			QuestManager.stopQuest(player, quest);
			return true;
		}
		if(args.length == 1 && (args[0].equals("iteminfo") || args[0].equals("ii"))) {
			if(!pi.isStaff()) {
				sender.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous n'avez pas la permission.");
				return true;
			}
			ItemStack is = player.getInventory().getItemInMainHand();
			if(is == null) {
				sender.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Prenez un item en main.");
				return true;
			}
			sender.sendMessage(ChatColor.GRAY + is.getType().toString() + " = " + is.getType().getId() + ":" + is.getData().getData());
			return true;
		}
		if(args.length == 1 && args[0].equals("reload")) {
			if(!pi.hasPermission("quest.admin")) {
				sender.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous n'avez pas la permission.");
				return true;
			}
			SQLFunction.loadQuestList();
			for(PlayerQuest pq : Main.getPlayerList().values()) {
				pq.checkQuest();
			}
			sender.sendMessage(Main.getQuestPrefix() + ChatColor.GREEN + "Chargement effectu�.");
			
		} else if(args.length == 1 && args[0].equals("resync")) {
			if(!pi.hasPermission("quest.admin")) {
				sender.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous n'avez pas la permission.");
				return true;
			}
			for(EntityType et : EntityType.values()) {
				SQLFunction.addType(et);
			}
			sender.sendMessage(Main.getQuestPrefix() + ChatColor.GREEN + "Synchronisation des types d'entit�s effectu�e.");
		} else if(args.length == 1 && args[0].equals("reset")) {
			if(!pi.hasPermission("quest.admin")) {
				sender.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous n'avez pas la permission.");
				return true;
			}
			SQLFunction.reset(player);
			sender.sendMessage(Main.getQuestPrefix() + "Vos qu�tes sont r�initialis�es.");
			
		} else if(args.length == 2 && args[0].equals("forcecomplete")) {
			if(!pi.hasPermission("quest.admin")) {
				sender.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous n'avez pas la permission.");
				return true;
			}
			Integer quest_id = null;
			try {
				quest_id = Integer.parseInt(args[1]);
			} catch(Exception ex) {
				sender.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "ID de qu�te incorrect.");
				return true;
			}
			Quest quest = Main.getQuestList().get(quest_id);
			if(quest == null) {
				sender.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Qu�te inconnue.");
				return true;
			}
			QuestManager.endOfPlayerQuest(player, quest);
		} else if(args.length >= 3 && args[0].equals("talknpc")) {
			NPCQuest npcquest = Main.getNpcQuestList().get(Integer.parseInt(args[1]));
			if(npcquest == null) {
				sender.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Veuillez recommencer.");
				return true;
			}
			NPC npc = CitizensAPI.getNPCRegistry().getById(npcquest.getId());
			if(player.getLocation().distance(npc.getStoredLocation()) > 5) {
				sender.sendMessage(Main.getQuestPrefix() + ChatColor.RED + "Vous �tes trop �loign� pour parler.");
				return true;
			}
			if(args[2].equalsIgnoreCase("end")) {
				MenuManagement.openEnd(player, npcquest);
			} else if(args[2].equalsIgnoreCase("list")) {
				MenuManagement.openList(player, npcquest);
			} else if(args[2].equalsIgnoreCase("accept")) {
				Quest quest;
				try {
					quest = Main.getQuestList().get(Integer.parseInt(args[3]));
				} catch(Exception ex) {
					NPCAPI.talkTo(npc, player, "Je n'ai rien � vous proposer.");
					return true;
				}
				Main.getPlayerList().get(player).acceptQuest(quest);
			}
		} else {
			MenuManagement.openGlobal(player);
		}
		return true;
	}
}
